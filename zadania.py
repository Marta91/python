# -*- coding: UTF-8 -*-

def function1(word):
    length = 0
    for i in word:
        length += 1
    print length


function1('12345')


def function3(word):
    if len(word) <= 3:
        return None
    else:
        return word[:3] + word[len(word) - 3:]


print function3('123456789')


def function4(word):
    newWord = ''
    for i, x in enumerate(word):
        if x == word[0] and i != 0:
            x = '_'
        newWord += x
    return newWord


print function4('oksymoron')


def function5(firstWord, secondWord):
    return secondWord[0] + firstWord[1:] + ' ' + firstWord[0] + secondWord[1:]


print function5('python', 'zadanie')


def function6(word):
    if len(word) >= 3:
        if word[len(word) - 3:] == 'ing':
            word = word[:len(word) - 3] + 'ly'
        else:
            word += 'ing'
    return word


print function6('read')


def function7(text):
    nie = -1
    zly = -1
    lastSign = text[len(text) - 1]
    text = text[:len(text) - 1]
    for i, ch in enumerate(text.split()):
        if ch == 'nie':
            nie = i
        if ch == 'zly' or ch == 'zly':
            zly = i
    if zly < 0 or nie < 0:
        return 'nie wymaga zmiany'
    if nie < zly:
        text = text.split()[:nie]
        text.append('nienajgorszy')
        text = ' '.join(text)
        text += lastSign
    return text


print function7('tekst jest nie taki zly!')


def function8(wordList):
    theLongest = (len(wordList[0]), wordList[0])
    for i in wordList:
        if len(i) > theLongest[0]:
            theLongest = (len(i), i)
    return theLongest


print function8(['siema', 'co', 'tam', 'ciekawego', 'slychac'])  # zwraca poki co jedno najdluzsze slowo


def function9(word, number):
    if number >= len(word) or number < 0:
        print 'Drugi podany parametr jest nieprawidlowy'
    else:
        wordList = []
        for item in word:
            wordList.append(item)
        wordList.pop(number)
        wordList = ''.join(wordList)
        print wordList


function9('python', 5)


def function10(word):
    word = word[len(word) - 1] + word[1:len(word) - 1] + word[0]
    return word


print function10('python')


def function11(word):
    newWord = ''
    for i, ch in enumerate(word):
        if i % 2 == 0:
            newWord += ch
    return newWord


print function11('012345678')


def function12(text):
    dict = {}
    for i in text.split():
        if i not in dict:
            dict[i] = 1
        else:
            dict[i] = dict[i] + 1
    return dict


print function12('co sie tam wyprawia tam tam wyprawia co')


def function13():
    text = raw_input('Wprowadz tekst: ')
    print text.upper()
    print text.lower()


# function13() dziala, wylaczona, zeby nie wpisywac ciagle

def function14(numbersList):
    theLargest = numbersList[0]
    for i in numbersList:
        if i > theLargest:
            theLargest = i
    return theLargest


print function14([5, 3, -8, 10])


def function15(wordList):
    count = 0
    for item in wordList:
        if len(item) > 2 and item[0] == item[len(item) - 1]:
            count += 1
    return count


print function15(['kajak', 'woda', 'auto', 'radar'])


def function16WithSet(entryList):
    newList = list(set(entryList))
    return newList


print function16WithSet([1, 2, 3, 2, 3, 2, 4])


def function16WithoutSet(entryList):
    newList = []
    for item in entryList:
        if item not in newList:
            newList.append(item)
    return newList


print function16WithoutSet([1, 2, 3, 2, 3, 2, 4])


def function17(entryList):
    newList = list(entryList)
    return newList


print function17([1, 'python', 12, 5])


def function18(numberOfChar, text):
    newList = []
    for item in text.split():
        if len(item) > numberOfChar:
            newList.append(item)
    return newList


print function18(3, 'kiedys wybiore sie do lasu')


def function19(firstList, secondList):
    for item in firstList:
        if item in secondList:
            return True
    return False


print function19([1, 2, 3], [5, 6, 2])


def function20():
    dict = {}
    for x in range(1, 11):
        dict[x] = x * x
    return dict


print function20()


def function21(firstDict, secondDict):  # nie wiem czy o to chodzi
    newDict = {}
    newDict['nowy'] = [firstDict, secondDict]
    return newDict


print function21({'a': 1, 'b': 2}, {'c': 3, 'd': 4})


def function22(n):
    for x in range(n + 1):
        print '*' * x
    n -= 1
    while n > 0:
        print '*' * n
        n -= 1


function22(5)


def function23(word):
    print word[::-1]


function23('python')


def function24(entryTuple):
    countOdd = 0
    countOddSum = 0
    countEven = 0
    countEvenSum = 0
    for item in entryTuple:
        if item % 2 == 0:
            countEven += 1
            countEvenSum += item
        else:
            countOdd += 1
            countOddSum += item
    print 'Liczba cyfr nieparzystych: %s, ich suma: %s \n' \
          'Liczba cyfr parzystych: %s, ich suma: %s' % (countOdd, countOddSum, countEven, countEvenSum)


function24((3, 7, 2, 4, 1))


def function25(entryTuple):
    for item in entryTuple:
        print item, type(item)


function25((3, [3, 2], 3.56, {'klucz': 'wartosc'}, ('a', 4, 7)))


def function26(n):
    for item in range(1, n + 1):
        if item % 3 != 0 and item % 4 != 0:
            print item,


function26(10)


def function27():
    countLetters = 0
    couuntDigits = 0
    word = unicode(raw_input('Podaj ciag znakow: '))
    for item in word:
        if item.isnumeric():
            couuntDigits += 1
        if item.isalpha():
            countLetters += 1
    print 'Ilosc wystepujacych liter: %s \nIlosc wystepujacych cyfr: %s' % (countLetters, couuntDigits)


# function27()

def function28():
    import re
    password = raw_input('Podaj haslo: ')
    if len(password) < 8:
        print 'haslo jest za krotkie'
        return
    if len(password) > 16:
        print 'haslo jest zbyt dlugie'
        return
    if re.match(r'[A-Za-z0-9@#$%^&+=]', password):
        print 'prawidlowe haslo'
    else:
        print 'haslo za slabe'


# function28()

def function29():
    email = raw_input('Podaj adres email: ')
    countSign = 0
    signPosition = 0
    dotPosition = 0
    if email[len(email) - 1] == '@':
        print 'Email nie moze konczyc sie znakiem @'
        return
    for i, ch in enumerate(email):
        if ch == '@':
            countSign += 1
            signPosition = i
        if ch == '.':
            dotPosition = i
    if countSign == 1 and signPosition < dotPosition:
        print 'Email prawidlowy'
        return
    else:
        print 'Nieprawidlowy email'


# function29()

finalSum = 0


def function30():
    def add(lista):
        addFunc(lista)
        return finalSum

    def addFunc(lista):
        for elem in lista:
            if type(elem) is list:
                addFunc(elem)
            else:
                global finalSum
                finalSum += elem

    print(add([1, 2, [3, 4], [5, [6, 7]]]))


function30()


def function31(a, b):
    if b == 0:
        return 1
    if a == 0:
        return 0
    if b > 1:
        return a * function31(a, b - 1)
    else:
        return a


print function31(2, 3)


def function32():
    text = raw_input('Wprowadz ciag znakow: ')
    text = ' '.join(text.split()[::-1])
    print text


# function32()

def function33():
    class MyClass(object):

        text = ''

        def wczytaj(self):
            self.text = raw_input('Podaj ciag znakow: ')

        def wyswietl(self, how):
            if how == 'none':
                print(self.text)
            elif how == 'duze':
                print(self.text.upper())
            else:
                print(self.text.lower())

    myString = MyClass()
    myString.wczytaj()
    myString.wyswietl('male')
    myString.wyswietl('duze')
    myString.wyswietl('none')


# function33()

def function34():
    class Prostokat(object):
        def __init__(self, wysokosc, szerokosc):
            self.wysokosc = wysokosc
            self.szerokosc = szerokosc

        def obliczPole(self):
            print 'Pole wynosi: %s' % (self.wysokosc * self.szerokosc)

        def obliczObwod(self):
            print 'Obwod wynosi: %s' % (self.wysokosc * 2 + self.szerokosc * 2)

    prostokat1 = Prostokat(10, 20)

    prostokat1.obliczObwod()
    prostokat1.obliczPole()


function34()


def function35():
    class Prostokat(object):
        def __init__(self, wysokosc, szerokosc, *glebokosc):
            self.wysokosc = wysokosc
            self.szerokosc = szerokosc
            self.glebokosc = glebokosc

        def obliczPole(self):
            try:
                print 'Objętosc wynosi: %s' % (self.wysokosc * self.szerokosc * self.glebokosc[0])
            except IndexError:
                print 'Pole wynosi: %s' % (self.wysokosc * self.szerokosc)

        def obliczObwod(self):
            try:
                print 'Dlugosc krawedzi prostopadloscianu wynosi: %s' % (
                    self.wysokosc * 2 + self.szerokosc * 2 + self.glebokosc[0] * 4)
            except IndexError:
                print 'Obwod wynosi: %s' % (self.wysokosc * 2 + self.szerokosc * 2)

    p1 = Prostokat(10, 20)
    p2 = Prostokat(10, 20, 10)
    p1.obliczObwod()
    p1.obliczPole()
    p2.obliczObwod()
    p2.obliczPole()


function35()


def function36(numberOfHeads, numberOfLegs):
    numberOfHens = (4 * numberOfHeads - numberOfLegs) / 2
    numberOfRabbits = numberOfHeads - numberOfHens

    print 'Liczba kur: %s' % numberOfHens
    print 'Liczba krolikow: %s' % numberOfRabbits


function36(35, 94)


def function37(nameOfStreet, lastNumber):
    start = 1
    while start <= lastNumber:
        for x in range(1, 7):
            print '%s %s%s/%s' % (nameOfStreet, start, 'A', x)
        for x in range(1, 7):
            print '%s %s%s/%s' % (nameOfStreet, start, 'B', x)
        start += 2

function37('Pomorska', 6)

def function38(text):
    if type(text) is int:
        text = str(text)
    if type(text) is str:
        text = text.replace(' ', '')
    if type(text) is str or type(text) is list:
        if text == text[::-1]:
            print 'Podamy argument jest palindromem'
        else:
            print 'Podany argument nie jest palindromem'


function38('kobyla ma maly bok')


def function39(pesel):
    pesel = str(pesel)
    checkDigit = int(pesel[0]) + 3 * int(pesel[1]) + 7 * int(pesel[2]) + 9 * int(pesel[3]) + int(pesel[4]) + 3 * int(
        pesel[5]) + 7 * int(pesel[6]) + 9 * int(pesel[7]) + int(pesel[8]) + 3 * int(pesel[9])
    checkDigit = str(checkDigit)
    if checkDigit[len(checkDigit) - 1] == str(int(pesel[10]) % 10):
        print 'Pesel prawidlowy \nData urodzenia: %s%s.%s%s \nOsoba jest %s' % (
            pesel[4], pesel[5], pesel[6], pesel[7], 'mezczyzna' if int(pesel[9]) % 2 != 0 else 'kobieta')
    else:
        print 'Pesel nieprawidlowy'
